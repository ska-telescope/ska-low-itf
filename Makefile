PROJECT = ska-low-itf

# only publish specific chart
# HELM_CHARTS_TO_PUBLISH=ska-low-itf-environment

VALUES ?=

DOCS_SPHINXOPTS=-W --keep-going

-include PrivateRules.mak

include .make/base.mk

include .make/helm.mk

K8S_CHARTS := ska-low-itf-sut ska-low-itf-environment

include .make/k8s.mk

deploy-te: vault-login
	helmfile sync

vault-login:
	vault token lookup -address https://vault.skao.int \
	|| vault login -address https://vault.skao.int -method=oidc
