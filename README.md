# ska-low-itf

This projects contains the helm charts to be deployed into the SKA LOW ITF.

* ska-low-itf-environment
* ...

## Documentation

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-low-itf/badge/?version=latest)](https://developer.skao.int/projects/ska-low-itf/en/latest/?badge=latest)

The documentation for this project, including how to get started with it, can be found in the `docs` folder, or browsed in the SKA development portal:

* [ska-low-itf documentation](https://developer.skatelescope.org/projects/ska-low-itf/en/latest/index.html "SKA Developer Portal: ska-low-itf documentation")