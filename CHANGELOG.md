# Version History

## unreleased
* [LOW-1012] Add antenna latitudes and longitudes to tmdata
* [LOW-991] Decouple stations from array
* [LOW-952] Move tmdata from helmfile folder
* [SPRS-224] Correct antenna polarisation labels
* [MCCS-2202] Specify NADs instead of interfaces and annotations
* [MCCS-2189] Update DAQ spec
* [LOW-908] Add cabinetbanks key to platform spec
* [LOW-879] Change Tango device naming conventiosn to align with other environments,
  add antennas to the station specs, and add a csp_ingest_ip for each station.
* [LOW-856] Correct station SDN IPs
* [MCCS-2120] DAQ storage claims
* [LOW-796] Update jupyterhub volume mounts
* [MCCS-2103] Add gateway support to platform specs
* [LOW-796] Remove SUT chart
* [LOW-802] Add grafana EDA attributes
* [LOW-801] Deploy alarmhandler, TMC and CSP/CBF configuration
* [LOW-799] Portal with signal display
* [LOW-762] Update tmc-low to 0.14.0
* [MCCS-2070] Use string keys in platform specs
* [LOW-793] Update to SDP 0.19.0
* [LOW-788] Add SDP to EDA
* [LOW-778] Update jupyterhub image
* [MCCS-2066] Station address handling
* [LOW-759] Grafana configmap
* [LOW-758] SKA portal auto templating
* [LOW-755] Update ska-low-mccs-spshw for bugfix
* [LOW-751] Linting packages
* [LOW-750] Jupyterhub cell time tracking
* [LOW-684] Portal deployment update
* [LOW-726] Spead2 hack in Dockerfile
* [MCCS-1975] Cabinet subnet handling
* [LOW-722] SDP PV jupyterhub access
* [LOW-705] Deploy jupyterhub instance
* [LOW-704] Fix delay polynomial device values
* [LOW-698] Add python-casacore to jupyterhub image
* [LOW-689] Add ska-portal to test-environment
* [LOW-686] Add tempo2 to jupyterhub image
* [LOW-658] SDP deployments pod
* [LOW-671] Add tempo to jupyterhub image
* [LOW-668] Add psrcat to jupyterhub image
* [LOW-631] CBF configuration
* [LOW-630] DAQ storage in Low ITF shared data volume
* [LOW-628] add new ITF PDUs
* [LOW-626] Add CNIC PVC and PV templates
* [LOW-611] Create test PVC
* [LOW-608] CBF p4-metrics network attachment definition and config
* [LOW-604] Deploy CNIC
* [LOW-597] anonymous viewer role in Grafana
* [LOW-596] fetch EDA password of Vault
* [LOW-556] Add jupyterhub-h5web
* [LOW-583] Integrate SDP charts into SUT
* [LOW-538] Add backups of Grafana and EDA configs
* [LOW-568] Update PDU IP addresses
* [LOW-501] add EDA for SUT
* [KCCS-1683] Add Low ITF platform spec
* [LOW-501] Add support for Grafana
* [LOW-501] Add support for ska-tango-archiver
* [LOW-493] switches dashboard

## 0.1.0
* [LOW-519] update ska-low-itf-environment to latest released versions
* [LOW-519] enable Tango DB persistent volume
* [LOW-519] pin SPS devices to au-itf-cloud03
* [LOW-519] grant binderhub default SA read access to test-environment and sut namespaces
* [LOW-519] add ska-low-itf-sut chart and values
* [LOW-491] build a base image for JupyterHub user pods
* [MCCS-1636] Use ska-ser-sphinx-theme for documentation
* [LOW-323] Initial release of ska-low-itf


