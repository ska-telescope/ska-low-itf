Introduction
============

This project contains the (helm) charts to install software to the LOW-ITF

Testing
-------

Testing is done in other projects

Dashboards
----------

The repository also contains Taranta dashboards, which are the GUI to Tango devices. These can be found in `resources/dashboards` and can be uploaded to Taranta in the LOW-ITF.

Jupyter Notebooks
-----------------

Jupyter notebooks are interactive python scripts which can be used to develop test procedures, for prototyping or some advanced data analysis.
These can be found in `resources/notebooks` and added to the juypterhub running in the LOW-ITF.
